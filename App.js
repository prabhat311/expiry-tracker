import React, { Component } from 'react';
import {
  createAppContainer,
  createStackNavigator,
  createDrawerNavigator,
  createSwitchNavigator
} from 'react-navigation';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Provider } from 'react-redux';
import HomeScreen from './src/screens/HomeScreen';
import store from './src/redux/store';
import FormScreen from "./src/screens/FormScreen";
import LoginScreen from "./src/screens/LoginScreen";
import * as firebase from 'firebase/app';
import {firebaseConfig} from "./frebaseConfig";
import LoadingScreen from "./src/screens/LoadingScreen";
import SettingsScreen from "./src/screens/SettingsScreen";
import * as BackgroundFetch from 'expo-background-fetch';
import * as TaskManager from 'expo-task-manager';
import {Notifications} from 'expo';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

const taskName = 'test-background-fetch';
TaskManager.defineTask(taskName, async () => {
  const localNotification = {
    title: 'ITS BEEN 2 MINUTES',
    body: 'done!'
  };

  const schedulingOptions = {
    time: (new Date()).getTime() + 5000
  };

  // Notifications show only when app is not active.
  // (ie. another app being used or device's screen is locked)
  Notifications.scheduleLocalNotificationAsync(
      localNotification, schedulingOptions
  );

  return BackgroundFetch.Result.NewData;
});

const options = {
  minimumInterval: 2 * 60 // in seconds
};


class App extends Component {

  constructor() {
    super();
    this.initializeFirebase();
  }

  initializeFirebase = () => {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
  };


  handleNotification() {
    console.warn('ok! got your notif');
  }


  async componentDidMount() {
    let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);

    if (Constants.isDevice && result.status === 'granted') {
      console.log('Notification permissions granted.')
    }

    // If we want to do something with the notification when the app
    // is active, we need to listen to notification events and
    // handle them in a callback
    Notifications.addListener(this.handleNotification);

    this.registerTaskAsync();
  }

  registerTaskAsync = async () => {
    await BackgroundFetch.registerTaskAsync(taskName, options);
    alert('task registered');
  };


  render() {
    return (
        <Provider store={store}>
            <AppContainer />
        </Provider>
    );
  }
}

const HomeNavigator = createStackNavigator({
  list: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerLeft: (
            <MaterialCommunityIcons
                name="menu"
                size={30}
                color='#bebebe'
                onPress={() => navigation.openDrawer()}
                style={{ marginLeft: 10 }}
            />
        ),
        title: 'My list',
      };
    }
  },
  form: {
    screen: FormScreen,
    navigationOptions: () => ({
      title: 'Add product',
    })
  }
});

const AppDrawerNavigator = createDrawerNavigator(
    {
      HomeNavigator: {
        screen: HomeNavigator,
        navigationOptions: {
          title: 'Home',
          drawerIcon: () => <MaterialCommunityIcons name="home" size={24} />
        }
      },
      SettingsScreen: {
        screen: SettingsScreen,
        navigationOptions: {
          title: 'Settings',
          drawerIcon: () => <MaterialCommunityIcons name="settings" size={24} />
        }
      }
    }
);

const LoginNavigator = createSwitchNavigator({
  LoadingScreen,
  LoginScreen,
  AppDrawerNavigator
});

const AppContainer = createAppContainer(LoginNavigator);

export default App;

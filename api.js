import moment from 'moment';

export function formatDate(dateString) {
  const parsed = moment(new Date(dateString));

  if (!parsed.isValid()) {
    return dateString;
  }

  return parsed.format('D MMM YYYY');
}

export function getCountdownParts(eventDate) {
  const duration = moment.duration(moment(new Date(eventDate)).diff(new Date()));
  return parseInt(duration.as('days') + 1);
}

export function compareDates(eventDate1, eventDate2) {
  const duration = moment.duration(moment(new Date(eventDate1)).diff(eventDate2));
  return duration;
}

export function formatDateTime(dateString) {
  const parsed = moment(new Date(dateString));

  if (!parsed.isValid()) {
    return dateString;
  }

  return parsed.format('H A on D MMM YYYY');
}

export function getIconData(category) {
  let icon = {};
  switch(category) {
    case 'Food':  icon.name = 'food';
                  icon.color = '#ffb92c';
                  return icon;
    case 'Medical':  icon.name = 'medical-bag';
      icon.color = '#87D7CA';
      return icon;
    case 'Household':  icon.name = 'spray-bottle';
      icon.color = '#eb4984';
      return icon;
    case 'Document':  icon.name = 'file-document';
      icon.color = '#875fc0';
      return icon;
    default: return icon;
  }
}
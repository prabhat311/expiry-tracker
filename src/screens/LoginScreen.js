import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as actions from "../redux/actions";
import {View, StyleSheet, Text, TextInput, TouchableHighlight} from "react-native";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#f8faf9',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        margin: 0
    },
    appTitle: {
        fontSize: 28,
        fontWeight: '400',
        color: '#2eb67d',
        alignSelf: 'center'
    },
    formContainer: {
        flex: 1
    },
    fieldItem: {
        width: 250,
        margin: 10,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#bebebe'
    },
    text: {
        height: 50,
        // borderWidth: 1,
        margin: 0,
        marginLeft: 7,
        marginRight: 7,
        paddingLeft: 10,
    },
    button: {
        height: 50,
        backgroundColor: '#47c4f2',
        borderColor: '#47c4f2',
        alignSelf: 'stretch',
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonText: {
        color: '#fff',
        fontSize: 18,
    }
});

class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isLoading: false
        };
    }

    onSignIn = async () => {
        if (this.state.email && this.state.password) {
            this.setState({ isLoading: true });
            try {
                const response = await firebase
                    .auth()
                    .signInWithEmailAndPassword(this.state.email, this.state.password);
                if (response) {
                    this.setState({ isLoading: false });
                    this.props.navigation.navigate('LoadingScreen');
                }
            } catch (error) {
                this.setState({ isLoading: false });
                switch (error.code) {
                    case 'auth/user-not-found':
                        this.onSignUp();
                        break;
                    case 'auth/invalid-email':
                        alert('Please enter an email address');
                }
            }
        }
    };

    onSignUp = async () => {
        if (this.state.email && this.state.password) {
            this.setState({ isLoading: true });
            try {
                const response = await firebase
                    .auth()
                    .createUserWithEmailAndPassword(
                        this.state.email,
                        this.state.password
                    );
                if (response) {
                    this.setState({ isLoading: false });
                    const user = await firebase
                        .database()
                        .ref('users')
                        .child(response.user.uid)
                        .set({ email: response.user.email, uid: response.user.uid });

                    this.props.navigation.navigate('LoadingScreen');
                    //automatically signs in the user
                }
            } catch (error) {
                this.setState({ isLoading: false });
                if (error.code == 'auth/email-already-in-use') {
                    alert('User already exists.Try loggin in');
                }
                console.log(error);
            }
        } else {
            alert('Please enter email and password');
        }
    };

    render() {
        return (
            <View style={styles.body}>
                <View style={styles.logoContainer}>
                    <View style={styles.logo}>
                        <MaterialCommunityIcons name={'calendar-check'} size={150} color={'#2eb67d'} />
                    </View>
                    <View>
                        <Text style={styles.appTitle}>Expiry Tracker</Text>
                    </View>
                </View>
                <View style={styles.formContainer}>
                    <View style={styles.fieldItem}>
                        <TextInput
                            style={styles.text}
                            placeholder={'abc@example.com'}
                            placeholderTextColor='#bebebe'
                            keyboardType="email-address"
                            onChangeText={email => this.setState({ email })}
                        />
                    </View>
                    <View style={styles.fieldItem}>
                        <TextInput
                            style={styles.text}
                            placeholder="Password"
                            placeholderTextColor='#bebebe'
                            secureTextEntry
                            onChangeText={password => this.setState({ password })}
                        />
                    </View>
                    <TouchableHighlight
                        onPress={this.onSignIn}
                        style={styles.button}>
                        <Text style={styles.buttonText}>LOGIN</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        events: state.entries.events
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(LoginScreen);
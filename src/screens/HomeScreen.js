import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as actions from "../redux/actions";
import EntryList from '../components/EntryList';
import ActionButton from 'react-native-action-button';

class HomeScreen extends Component {

    constructor() {
        super();
        this.state = {
            events:[],
            currentUser:""
        }
    }

    handleAddEvent = () => {
        this.props.navigation.navigate('form')
    }

    componentDidMount() {
        const currentUser = this.props.navigation.dangerouslyGetParent().getParam('user').uid;

        this.setState({
            currentUser: currentUser
        });

        this.props.startLoadingEntries(currentUser);
    }

    render() {
        return [
            <EntryList {...this.props} currentUser = {this.state.currentUser}/>,
            <ActionButton key="fab" buttonColor="rgba(231,76,60,1)" onPress={this.handleAddEvent}/>
        ]
    }
}

function mapStateToProps(state) {
    return {
        events: state.entries.events
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);
import React, { Component } from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';

import * as firebase from 'firebase/app';
import 'firebase/auth';

class SettingScreen extends Component {
  signOut = async () => {
    try {
      await firebase.auth().signOut();
      this.props.navigation.navigate('LoginScreen');
    } catch (error) {
      alert('Unable to sign out right now');
    }
  };

  render() {
    return (
      <View style={styles.container}>
          <TouchableHighlight
              onPress={this.signOut}
              style={styles.button}
          >
              <Text style={styles.buttonText}>LOGOUT</Text>
          </TouchableHighlight>
      </View>
    );
  }
}
export default SettingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f8faf9'
  },
    button: {
        height: 50,
        backgroundColor: '#47c4f2',
        borderColor: '#47c4f2',
        alignSelf: 'stretch',
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonText: {
        color: '#fff',
        fontSize: 18,
    }
});

import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as actions from "../redux/actions";
import EntryForm from '../components/EntryForm';

class FormScreen extends Component {
    render() {
        return [
            <EntryForm {...this.props}/>,
        ]
    }
}

function mapStateToProps(state) {
    return {
        events: state.entries.events
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(FormScreen);
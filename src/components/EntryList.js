import React from 'react';
import {
    StyleSheet,
    FlatList,
} from 'react-native';
import {compareDates} from '../../api';

import EntryCard from './EntryCard';

const styles = StyleSheet.create({
    list: {
        flex: 1,
        paddingTop: 5,
        backgroundColor: '#f8faf9'
    },
});

function EntryList(props) {
    const events = props.events;

    if(events && events.length > 1) {
        events.sort((x, y) => {
                return compareDates(x.date, y.date);
            }
        )
    }

    return  <FlatList
        key="flatlist"
        data = {events}
        currentUser = {props.currentUser}
        style={styles.list}
        keyExtractor={item => item.id}
        renderItem = {({ item, index }) => (
            <EntryCard
                event={item} index={index} {...props}
            />
        )}
    />
}

export default EntryList;
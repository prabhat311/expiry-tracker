import React, { Component } from 'react';
import {
    Picker,
    TextInput,
    View,
    Text,
    StyleSheet,
    TouchableHighlight,
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { formatDate } from '../../api';



const styles = StyleSheet.create({
    fieldContainer: {
        marginTop: 20,
        marginBottom: 20
    },
    fieldItem: {
        margin: 10,
        backgroundColor: '#f8faf9'
    },
    text: {
        height: 40,
        // borderWidth: 1,
        margin: 0,
        marginLeft: 7,
        marginRight: 7,
        paddingLeft: 10,
    },
    button: {
        height: 50,
        backgroundColor: '#47c4f2',
        borderColor: '#47c4f2',
        alignSelf: 'stretch',
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonText: {
        color: '#fff',
        fontSize: 18,
    },
});

class EntryForm extends Component {
    state = {
        title: "",
        date: "",
        currentUser: ""
    };

    constructor(props) {
        super(props);
        this.props = props;
    }

    async componentDidMount() {
        const currentUser = this.props.navigation.dangerouslyGetParent().getParam('user').uid;

        this.setState({
            currentUser: currentUser
        });
    }


    handleChangeTitle = (text) => {
        this.setState({
            title: text,
        });
    }

    handleDatePicked = (date) => {
        this.setState({
            date,
        });

        this.handleDatePickerHide();
    }


    handleDatePickerHide = () => {
        this.setState({
            showDatePicker: false,
        });
    }

    handleDatePress = () => {
        this.setState({
            showDatePicker: true,
        });
    }

    handleAddPress = () => {
        /*saveEvent(this.state)
            .then(() => {
                this.props.navigation.goBack();
            })*/
        //EntryList.updateState(this.state);
        const entry = {
          title: this.state.title,
          date: this.state.date.toString(),
          category: this.state.category
        };

        this.props.startAddingEntry(entry, this.state.currentUser);
        this.props.navigation.goBack();

    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                }}
            >
                <View style={styles.fieldContainer}>
                    <View style={styles.fieldItem}>
                        <TextInput
                            style={styles.text}
                            onChangeText={this.handleChangeTitle}
                            placeholder="Product name"
                            placeholdercolor = '#bebebe'
                            spellCheck={false}
                            value={this.state.title}
                        />
                    </View>
                    <View style={styles.fieldItem}>
                        <Picker
                            selectedValue={this.state.category}
                            style={styles.text}
                            mode='dropdown'
                            onValueChange={(itemValue, itemIndex) => {
                                if(itemValue !== "0") {
                                    this.setState({category: itemValue});
                                }
                                else {
                                    alert("Please select product category.");
                                }
                            }
                            }>
                            <Picker.Item color='#bebebe' label="Category" value="0" />
                            <Picker.Item label="Food" value="Food" />
                            <Picker.Item label="Medical" value="Medical" />
                            <Picker.Item label="Household" value="Household" />
                            <Picker.Item label="Document" value="Document" />
                        </Picker>
                    </View>
                    <View style={styles.fieldItem}>
                        <TextInput
                            style={styles.text}
                            placeholder="Expiry date"
                            placeholdercolor = '#bebebe'
                            spellCheck={false}
                            value={formatDate(this.state.date.toString())}
                            editable={!this.state.showDatePicker}
                            onFocus={this.handleDatePress}
                        />
                        <DateTimePicker
                            isVisible={this.state.showDatePicker}
                            mode="date"
                            onConfirm={this.handleDatePicked}
                            onCancel={this.handleDatePickerHide}
                        />
                    </View>
                </View>

                <TouchableHighlight
                    onPress={this.handleAddPress}
                    style={styles.button}
                >
                    <Text style={styles.buttonText}>Add</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

export default EntryForm;
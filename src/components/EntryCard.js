import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {
    formatDate,
    getIconData,
    getCountdownParts,
} from '../../api';



const styles = StyleSheet.create({
    card: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        paddingTop: 10,
        paddingBottom: 10,
        margin: 10,
        marginTop: 5,
        marginBottom: 5,
    },
    cardImage: {
        flex:1,
        alignSelf: 'center'
    },
    cardText: {
        flex: 7,
    },
    cardHeader: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-start'
    },
    date: {
        fontWeight: '200',
        fontSize: 14,
        flex: 3,
        color: '#a3a3a3',
    },
    title: {
        fontSize: 22,
        fontWeight: '300',
        marginLeft: 7,
        flex: 5
    },
    delete: {
      flex: 1,
      alignSelf: 'center'
    },
    counterContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems:'flex-end',
        marginLeft: 7
    },
    counterText: {
        fontSize: 18,
        color: '#a0a0a0',
    },
    counterLabel: {
        fontSize: 16,
        fontWeight: '300',
        color: '#a3a3a3',
    },
});

class EntryCard extends React.Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.handleDeleteAction = this.handleDeleteAction.bind(this);
    };

    handleDeleteAction() {
        this.props.startRemovingEntry(this.props.index, this.props.currentUser, this.props.event);
    };

    render() {
        let state = "good";
        let days =  getCountdownParts(this.props.event.date);

        if(days === 0) {
            state = "expiring";
        }
        else if (days < 0) {
            state = "expired";
        }

        const iconData = getIconData(this.props.event.category);
        return (
            <View style={styles.card}>
                <View style={styles.cardImage}>
                    <MaterialCommunityIcons name={iconData.name} size={42} color={iconData.color} />
                </View>
                <View style={styles.cardText}>
                    <View style={styles.cardHeader}>
                        <Text style={styles.title}>{this.props.event.title}</Text>
                        <Text style={styles.date}>{formatDate(this.props.event.date)}</Text>
                    </View>

                    {state === 'good' && (
                        <View style={styles.counterContainer}>
                            <Text style={styles.counterLabel}>Expires in </Text>
                            <Text style={styles.counterText}>{days}</Text>
                            <Text style={styles.counterLabel}> days</Text>
                        </View>
                    )}
                    {state === 'expiring' && (
                        <View style={styles.counterContainer}>
                            <Text style={styles.counterLabel}>Expires today</Text>
                        </View>
                    )}
                    {state === 'expired' && (
                        <View style={styles.counterContainer}>
                            <Text style={[styles.counterLabel, {color: 'red'}]}>Expired</Text>
                        </View>
                    )}

                </View>
                <TouchableHighlight onPress={this.handleDeleteAction}>
                    <View style={styles.delete}>
                        <MaterialCommunityIcons name={'trash-can-outline'} size={30} color={'#e74c3c'} />
                    </View>
                </TouchableHighlight>
            </View>
        );
    }
}

EntryCard.propTypes = {
    event: PropTypes.shape({
        title: PropTypes.string.isRequired,
        date: PropTypes.instanceOf(Date)
    }),
};

export default EntryCard;
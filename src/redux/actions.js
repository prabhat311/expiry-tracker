import * as firebase from 'firebase/app';
import 'firebase/database';

export function loadEntries(entries) {
    return {
        type: 'LOAD_ENTRIES',
        entries,
    }
}

export function addEntry(entry) {
    return {
        type: 'ADD_ENTRY',
        entry
    }

}

export function removeEntry(index) {
    return {
        type: 'REMOVE_ENTRY',
        index
    }

}

export function startAddingEntry(entry, user) {
    return (dispatch) => {
        return firebase.database().ref('entries').child(user).child('events').push(entry).then(() => {
            dispatch(addEntry(entry));
        }).catch((error) => {
            console.log(error);
        });
    }
}

export function startLoadingEntries(user) {
    return (dispatch) => {
        return firebase.database().ref('entries').child(user).once('value').then((snapshot) => {
            let entries = {events: []};
            snapshot.forEach((childSnapshot) => {
                entries['events'] = Object.values(childSnapshot.val());
            });
            //console.log(entries);
            dispatch(loadEntries(entries));
        }).catch((error) => {
            console.log(error);
        });
    }
}

export function startRemovingEntry(index, user, event) {
    return (dispatch) => {
        return firebase.database().ref('entries').child(user).once('value').then((snapshot) => {
            let entries = {events: []};
            snapshot.forEach((childSnapshot) => {
                entries['events'] = Object.values(childSnapshot.val());
            });

            entries['events'].forEach((storedEvent, idx) => {
                if(JSON.stringify(storedEvent) === JSON.stringify(event)) {
                    entries.events.splice(idx,1);
                }
            });
            console.log(entries);
            firebase.database().ref('entries').child(user).update(entries);
            //console.log(entries);
            dispatch(loadEntries(entries));
        }).catch((error) => {
            console.log(error);
        });
    }
}
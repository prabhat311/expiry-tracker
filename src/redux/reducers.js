import {combineReducers} from "redux";

const initialState = {
    events: []
};

function entries(state = {}, action) {
    switch(action.type) {
        case 'LOAD_ENTRIES': return action.entries;
        case 'ADD_ENTRY': return {events:[...state.events, action.entry]};
        case 'REMOVE_ENTRY': return {events:[...state.events.slice(0,action.index), ...state.events.slice(action.index+1)]};
        default: return state;
    }
}

const rootReducer = combineReducers({entries});

export default rootReducer;
